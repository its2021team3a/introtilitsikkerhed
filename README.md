# introtilitsikkerhed

Intro til IT-sikkerhed

- Navn på protokol: BOB  
- Navne på personer som er ansvarlige for protokollen: Gruppe 3 (os)  
- Versionsnummer: 1  
- Formål med protokollen (i letforståeligt sprog): returnere klokken via “sikkert” login  
    
- Teknisk beskrivelse. :   

	Client:  
		Connection: connect to port 33333  
		Send password  
		Lyt efter svar  
		Hvis false: disconnect  
		Ellers send “time” streng  
		Lyt efter svar  
 
	Authentication:  
		nøgle:  
			Bruger: BOBO  
			Cleartext password  


	Server:  
		Listening socket on port 33333  
		Lyt efter password  
		Check pass  
		Returner boolean resultat af check  
		Lyt efter kommando  
		Hvis kommando == “time” returner tiden.  



- Sikkerhedsniveau af protokol (er der kendte svagheder / problemer / fejl / validering af data)  
Bruteforce,   
