from socket import *
from datetime import datetime

HOST = '127.0.0.1'
PORT = 33333        # The port used by the server
PASSWORD = b'12345678'

serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind((HOST,PORT))
serverSocket.listen(1)
print('listening')
while 1:
    connectionSocket, addr = serverSocket.accept()
    recievedPassword = connectionSocket.recv(1024)
    print(recievedPassword)
    if (recievedPassword == PASSWORD):
        print('password correct')
        connectionSocket.sendall(b'1')
    recievedCommand = connectionSocket.recv(1024)
    if(recievedCommand == b'time'):
        current = datetime.now().strftime("%X")
        b = bytearray(current.encode('utf-8'))
        connectionSocket.sendall(b)